package com.example.hmizumoto.todoapp.data.entity.mapper;

import com.example.hmizumoto.todoapp.domain.model.TodoModel;
import com.example.hmizumoto.todoapp.data.repository.realm.Todo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;



@Singleton public class DBDataMapper {
    @Inject public DBDataMapper(){
    }

    public  TodoModel transform(Todo todo){
        TodoModel todoModel = null;
        if (todo != null){
            todoModel = new TodoModel(todo.getId());
            todoModel.setCreatedAt(todo.getCreatedAt());
            todoModel.setUpdatedAt(todo.getUpdatedAt());
            todoModel.setDone(todo.getDone());
            todoModel.setTitle(todo.gettitle());
        }
        return todoModel;
    }

    public Collection<TodoModel> transform(Collection<Todo> todos){
        List<TodoModel> todoModelList = new ArrayList<>();
        TodoModel todoModel;
        for (Todo todo: todos){
            todoModel = transform(todo);
            if (todo != null) {
                todoModelList.add(todoModel);
            }
        }
        return todoModelList;
    }
}
