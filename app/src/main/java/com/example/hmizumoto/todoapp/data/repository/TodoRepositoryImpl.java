package com.example.hmizumoto.todoapp.data.repository;

import android.content.Context;

import com.example.hmizumoto.todoapp.data.repository.datasource.TodoDataStore;
import com.example.hmizumoto.todoapp.data.repository.datasource.TodoDataStoreFactory;
import com.example.hmizumoto.todoapp.domain.model.TodoModel;
import com.example.hmizumoto.todoapp.domain.repository.TodoRepository;
import java.util.Collection;

import javax.inject.Inject;


public class TodoRepositoryImpl implements TodoRepository {
    private TodoDataStoreFactory todoDataStoreFactory;
    @Inject public TodoRepositoryImpl(TodoDataStoreFactory dataStoreFactory) {
        this.todoDataStoreFactory = dataStoreFactory;
    }
    @Override public void getTodoList(Context context, final TodoListCallback callback){
        final TodoDataStore todoDataStore = this.todoDataStoreFactory.createDBDataStore();
        todoDataStore.getTodoList(
        new TodoDataStore.TodoListCallback(){
            @Override public void onTodoListLoaded(Collection<TodoModel> todos) {
                callback.onTodoListLoaded(todos);
            }
        });
    }

    @Override public void createTodo(String title){
        final TodoDataStore todoDataStore = this.todoDataStoreFactory.createDBDataStore();
        todoDataStore.createTodo(title);

    }

    @Override public void upsertTodo(Long id, String title, boolean done){
        final TodoDataStore todoDataStore = this.todoDataStoreFactory.createDBDataStore();
        todoDataStore.upsertTodo(id, title, done);

    }

}
