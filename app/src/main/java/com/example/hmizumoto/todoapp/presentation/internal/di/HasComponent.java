package com.example.hmizumoto.todoapp.presentation.internal.di;

/**
 * Created by hmizumoto on 16/12/05.
 */

public interface HasComponent<C> {
    C getComponent();
}
