package com.example.hmizumoto.todoapp.domain.usecase;

import android.content.Context;

import com.example.hmizumoto.todoapp.domain.executor.ThreadExecutor;
import com.example.hmizumoto.todoapp.domain.model.TodoModel;
import com.example.hmizumoto.todoapp.domain.repository.TodoRepository;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Collection;

import javax.inject.Inject;

/**
 * Created by hmizumoto on 16/12/05.
 */

public class GetTodoListUseCaseImpl implements GetTodoListUseCase {
    private final TodoRepository.TodoListCallback callback = new TodoRepository.TodoListCallback() {
        @Override
        public void onTodoListLoaded(Collection<TodoModel> todos) {
            EventBus.getDefault().post(new MessageEvent(todos));
        }
    };
    private final TodoRepository todoRepository;
    private final ThreadExecutor threadExecutor;
    private final Context context;

    @Inject public GetTodoListUseCaseImpl(Context context, TodoRepository todoRepository, ThreadExecutor threadExecuter){
        this.todoRepository = todoRepository;
        this.threadExecutor = threadExecuter;
        this.context = context;
    }


    @Override public void execute(){
        this.threadExecutor.execute(this);
    }
    @Override public void run(){
        todoRepository.getTodoList(context, callback);

    }
}
