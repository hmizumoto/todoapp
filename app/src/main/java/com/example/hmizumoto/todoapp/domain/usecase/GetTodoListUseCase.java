package com.example.hmizumoto.todoapp.domain.usecase;

import com.example.hmizumoto.todoapp.domain.model.TodoModel;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Collection;

/**
 * Created by hmizumoto on 16/12/05.
 */

public interface GetTodoListUseCase extends UseCase {
    void execute();

    public class MessageEvent{
        public final Collection<TodoModel> todoModels;
        public MessageEvent(Collection<TodoModel> todoModels){
            this.todoModels = todoModels;
        }

    }

}
