package com.example.hmizumoto.todoapp.presentation.view;

/**
 * Created by hmizumoto on 16/12/05.
 */

import android.content.Context;

/**
 * Interface representing a View that will use to load data.
 */
public interface LoadDataView {

    /**
     * Get a {@link android.content.Context}.
     */
    Context getContext();
}