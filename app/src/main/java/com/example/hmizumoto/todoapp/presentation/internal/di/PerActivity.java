package com.example.hmizumoto.todoapp.presentation.internal.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.inject.Scope;

/**
 * Created by hmizumoto on 16/12/05.
 */

@Scope @Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {
}