package com.example.hmizumoto.todoapp.data.repository.realm;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hmizumoto on 16/12/02.
 */

public class Todo extends RealmObject{
    @PrimaryKey
    private long id;

    private String title;
    private Date createdAt;
    private Date updatedAt;
    private boolean done;

    public long getId() { return id; }
    public void setId(long id) { this.id = id; }

    public String gettitle() {
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt){
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt){
        this.updatedAt = updatedAt;
    }

    public boolean getDone(){
        return done;
    }

    public void setDone(boolean done){
        this.done = done;
    }

    public void toggleDone(){
        this.done = !this.done;
    }

}
