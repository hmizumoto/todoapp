package com.example.hmizumoto.todoapp.data.repository.datasource;

import com.example.hmizumoto.todoapp.data.repository.realm.Todo;
import com.example.hmizumoto.todoapp.domain.model.TodoModel;
import com.example.hmizumoto.todoapp.data.entity.mapper.DBDataMapper;

import android.content.Context;
import android.util.Log;


import java.util.Collection;
import java.util.Date;
import java.util.UUID;


import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by hmizumoto on 16/12/02.
 */


public class DBTodoDataStore implements TodoDataStore{
    private Realm realm;
    private DBDataMapper mapper;
    public DBTodoDataStore(Context context, DBDataMapper mapper){
        this.mapper = mapper;
        Realm.init(context);

    }
    @Override public void getTodoList(final TodoListCallback collback){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Todo> todos = realm.where(Todo.class).findAllSorted("createdAt", Sort.DESCENDING);

        Collection<TodoModel> todoModelList = this.mapper.transform(todos);
        Log.d("MyApp", "get todo called "+ todoModelList.size());
        realm.close();
        collback.onTodoListLoaded(todoModelList);
    }

    @Override public void createTodo(String title){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Date currentDate = new Date();
        Todo todo = realm.createObject(Todo.class, UUID.randomUUID().hashCode());
        todo.setTitle(title);
        todo.setDone(false);
        todo.setCreatedAt(currentDate);
        todo.setUpdatedAt(currentDate);
        realm.commitTransaction();
        realm.close();
        Log.d("MyApp", "create Todo finished");
    }

    @Override public void upsertTodo(Long id, String title, boolean done){
        Log.d("MyApp", ""+ id);
        if (id == null){
            createTodo(title);
        }else{
            Realm realm = Realm.getDefaultInstance();
            Date currentDate = new Date();
            realm.beginTransaction();
            Todo todo = realm.where(Todo.class).equalTo("id",id).findFirst();
            todo.setTitle(title);
            todo.setDone(done);
            todo.setUpdatedAt(currentDate);
            realm.commitTransaction();
            realm.close();
        }
    }

}
