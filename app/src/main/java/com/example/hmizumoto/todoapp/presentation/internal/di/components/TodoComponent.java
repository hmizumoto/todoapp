package com.example.hmizumoto.todoapp.presentation.internal.di.components;

import com.example.hmizumoto.todoapp.presentation.internal.di.PerActivity;
import com.example.hmizumoto.todoapp.presentation.internal.di.modules.ActivityModule;
import com.example.hmizumoto.todoapp.presentation.internal.di.modules.TodoModule;
import com.example.hmizumoto.todoapp.presentation.view.fragment.TodoListFragment;

import dagger.Component;


@PerActivity
@Component(dependencies = ApplicationComponent.class,
        modules = { ActivityModule.class, TodoModule.class })
public interface TodoComponent extends ActivityComponent {
    void inject(TodoListFragment todoListFragment);
}