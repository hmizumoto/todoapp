package com.example.hmizumoto.todoapp.domain.model;

/**
 * Created by hmizumoto on 16/12/02.
 */

import java.util.Date;

public class TodoModel {
    private final long id;
    private String title;
    private Date createdAt;
    private Date updatedAt;
    private boolean done;

    public TodoModel(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt){
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt){
        this.updatedAt = updatedAt;
    }

    public boolean getDone(){
        return done;
    }

    public void setDone(boolean done){
        this.done = done;
    }

    public void toggleDone(){
        this.done = !this.done;
    }

}
