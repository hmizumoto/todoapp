package com.example.hmizumoto.todoapp.presentation.view.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.hmizumoto.todoapp.R;
import com.example.hmizumoto.todoapp.domain.model.TodoModel;
import com.example.hmizumoto.todoapp.presentation.view.fragment.TodoListFragment;

import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TodoAdapter extends ArrayAdapter<TodoModel>{
    private SimpleDateFormat sdf;
    private TodoListFragment.TodoDoneHandler handler;
    public TodoAdapter(Context context, TodoListFragment.TodoDoneHandler handler){
        super(context, R.layout.item_todo);
        this.sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.handler = handler;
    }

    @Override
    public View getView(final int pos, View view,@NonNull final ViewGroup parent){
        ViewHolder holder;
        final TodoModel todoModel = getItem(pos);

        if (view == null || view.getTag() == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.item_todo, parent, false);
            holder = new ViewHolder(view);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.mDoneCheck.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                handler.onCheckboxClicked(todoModel);
            }
        });

        bindTodo(todoModel, holder);
        view.setTag(holder);

        return view;
    }


    private void bindTodo(TodoModel todoModel, ViewHolder holder) {

        holder.mTxtCreatedAt.setText(sdf.format(todoModel.getCreatedAt()));
        holder.mTxtTitle.setText(todoModel.getTitle());
        holder.mDoneCheck.setChecked(todoModel.getDone());
    }



    public void addAll(List<TodoModel> todoModels) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            super.addAll(todoModels);
        } else {
            for (TodoModel todoModel : todoModels) {
                add(todoModel);
            }
        }
        notifyDataSetChanged();
    }

    static class ViewHolder {
        @BindView(R.id.txt_title) TextView mTxtTitle;
        @BindView(R.id.txt_createdAt) TextView mTxtCreatedAt;
        @BindView(R.id.checkBox) CheckBox mDoneCheck;
        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }
}
