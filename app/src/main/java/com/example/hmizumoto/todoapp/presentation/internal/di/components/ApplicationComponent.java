package com.example.hmizumoto.todoapp.presentation.internal.di.components;

import android.content.Context;

import com.example.hmizumoto.todoapp.TodoApplication;
import com.example.hmizumoto.todoapp.domain.executor.ThreadExecutor;
import com.example.hmizumoto.todoapp.domain.repository.TodoRepository;
import com.example.hmizumoto.todoapp.presentation.internal.di.modules.ApplicationModule;
import com.example.hmizumoto.todoapp.presentation.view.activity.BaseActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by hmizumoto on 16/12/05.
 */


@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(TodoApplication androidApplication);

    void inject(BaseActivity baseActivity);

    //Exposed to sub-graphs.
    Context context();

    TodoRepository todoRepository();

    ThreadExecutor threadExecutor();
}