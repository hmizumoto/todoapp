package com.example.hmizumoto.todoapp.domain.usecase;

import com.example.hmizumoto.todoapp.domain.model.TodoModel;

import java.util.Collection;

/**
 * Created by hmizumoto on 16/12/05.
 */

public interface CreateTodoUseCase extends UseCase {
    void execute(Long id, String title, boolean done);
}
