package com.example.hmizumoto.todoapp.presentation.view.activity;

import android.os.Bundle;

import com.example.hmizumoto.todoapp.R;

/**
 * Created by hmizumoto on 16/12/05.
 */

public class MainActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TodoListActivity.start(this);
    }
}