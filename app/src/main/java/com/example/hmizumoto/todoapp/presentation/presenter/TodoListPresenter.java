package com.example.hmizumoto.todoapp.presentation.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.hmizumoto.todoapp.domain.model.TodoModel;
import com.example.hmizumoto.todoapp.domain.usecase.CreateTodoUseCase;
import com.example.hmizumoto.todoapp.domain.usecase.GetTodoListUseCase;
import com.example.hmizumoto.todoapp.presentation.internal.di.PerActivity;
import com.example.hmizumoto.todoapp.presentation.view.TodoListView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Collection;

import javax.inject.Inject;

/**
 * Created by hmizumoto on 16/12/05.
 */

@PerActivity
public class TodoListPresenter implements Presenter {
    private final GetTodoListUseCase getTodoListUseCase;
    private final CreateTodoUseCase createTodoUseCase;
    private TodoListView todoListView;

    @Inject
    public TodoListPresenter(GetTodoListUseCase getTodoListUseCase, CreateTodoUseCase createTodoUseCase){
        this.getTodoListUseCase = getTodoListUseCase;
        this.createTodoUseCase = createTodoUseCase;
    }

    public void setView(@NonNull TodoListView view){
        this.todoListView = view;
    }

    @Override public void resume() {
        EventBus.getDefault().register(this);
    }

    @Override public void pause() {
        EventBus.getDefault().unregister(this);
    }


    public void loadTodoList() {
        this.getTodoList();
    }

    private void getTodoList(){
        this.getTodoListUseCase.execute();
    }

    public void saveTodo(String title){
        this.createTodoUseCase.execute(null, title, false);
    }

    public void updateTodo(long id, String title, boolean done){
        this.createTodoUseCase.execute(id, title, done);
    }

    private void showTodoCollectionInView(Collection<TodoModel> todos) {
        this.todoListView.renderTodoList(todos);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void OnMessageEvent(GetTodoListUseCase.MessageEvent event) {
        Log.d("MyApp", "" + event.todoModels.size());
        showTodoCollectionInView(event.todoModels);
    }
}
