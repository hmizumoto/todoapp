package com.example.hmizumoto.todoapp.presentation.internal.di.modules;

import android.app.Activity;

import com.example.hmizumoto.todoapp.presentation.internal.di.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hmizumoto on 16/12/05.
 */

@Module public class ActivityModule {
    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    /**
     * Expose the activity to dependents in the graph.
     */
    @Provides
    @PerActivity
    Activity activity() {
        return this.activity;
    }
}
