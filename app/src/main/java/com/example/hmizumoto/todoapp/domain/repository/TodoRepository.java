package com.example.hmizumoto.todoapp.domain.repository;

/**
 * Created by hmizumoto on 16/12/02.
 */
import android.content.Context;

import com.example.hmizumoto.todoapp.domain.model.TodoModel;

import java.util.Collection;

public interface TodoRepository {
    void getTodoList(Context context, TodoListCallback callback);
    interface TodoListCallback {
        void onTodoListLoaded(Collection<TodoModel> todos);
    }
    void createTodo(String title);
    void upsertTodo(Long id, String title, boolean done);
}
