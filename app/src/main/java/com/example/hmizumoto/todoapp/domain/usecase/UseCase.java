package com.example.hmizumoto.todoapp.domain.usecase;

/**
 * Created by hmizumoto on 16/12/05.
 */

public interface UseCase extends Runnable {
    void run();
}
