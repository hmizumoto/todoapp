package com.example.hmizumoto.todoapp.presentation.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;

import com.example.hmizumoto.todoapp.R;
import com.example.hmizumoto.todoapp.presentation.internal.di.HasComponent;
import com.example.hmizumoto.todoapp.presentation.internal.di.components.DaggerTodoComponent;
import com.example.hmizumoto.todoapp.presentation.internal.di.components.TodoComponent;
import com.example.hmizumoto.todoapp.presentation.internal.di.modules.TodoModule;
import com.example.hmizumoto.todoapp.presentation.view.fragment.TodoListFragment;

/**
 * Created by hmizumoto on 16/12/05.
 */

public class TodoListActivity extends BaseActivity implements HasComponent<TodoComponent>, TodoListFragment.TodoListListener {
    private TodoComponent todoComponent;

    public static void start(Context context){
        Intent intent = new Intent(context, TodoListActivity.class);
        context.startActivity(intent);
    }

    @Override protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_list);
        initInjector();
        addFragment(R.id.container_todo_list, new TodoListFragment());
    }
    private void initInjector() {
        this.todoComponent = DaggerTodoComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .todoModule(new TodoModule())
                .build();
    }
    @Override public TodoComponent getComponent() {
        return todoComponent;
    }

}
