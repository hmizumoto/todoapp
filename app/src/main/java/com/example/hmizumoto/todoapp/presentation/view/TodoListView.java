package com.example.hmizumoto.todoapp.presentation.view;

import com.example.hmizumoto.todoapp.domain.model.TodoModel;

import java.util.Collection;

/**
 * Created by hmizumoto on 16/12/05.
 */

public interface TodoListView extends LoadDataView {
    void renderTodoList(Collection<TodoModel> tweetModelCollection);

}
