package com.example.hmizumoto.todoapp.presentation.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.example.hmizumoto.todoapp.presentation.internal.di.HasComponent;

/**
 * Created by hmizumoto on 16/12/05.
 */

public class BaseFragment extends Fragment {
    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }
}
