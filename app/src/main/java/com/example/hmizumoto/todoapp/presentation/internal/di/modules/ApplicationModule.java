package com.example.hmizumoto.todoapp.presentation.internal.di.modules;

import android.content.Context;

import com.example.hmizumoto.todoapp.TodoApplication;
import com.example.hmizumoto.todoapp.data.executor.JobExecutor;
import com.example.hmizumoto.todoapp.data.repository.TodoRepositoryImpl;
import com.example.hmizumoto.todoapp.domain.executor.ThreadExecutor;
import com.example.hmizumoto.todoapp.domain.repository.TodoRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hmizumoto on 16/12/05.
 */
@Module
public class ApplicationModule {
    private final TodoApplication app;

    public ApplicationModule(TodoApplication app) {
        this.app = app;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.app;
    }

    @Provides @Singleton
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }


    @Provides @Singleton
    TodoRepository provideTodoRepository(TodoRepositoryImpl todoRepository) {
        return todoRepository;
    }


}
