package com.example.hmizumoto.todoapp.domain.usecase;


import com.example.hmizumoto.todoapp.domain.executor.ThreadExecutor;
import com.example.hmizumoto.todoapp.domain.repository.TodoRepository;


import javax.inject.Inject;

/**
 * Created by hmizumoto on 16/12/05.
 */

public class CreateTodoUseCaseImpl implements CreateTodoUseCase {

    private final TodoRepository todoRepository;
    private final ThreadExecutor threadExecutor;
    private String title;

    @Inject
    public CreateTodoUseCaseImpl(TodoRepository todoRepository, ThreadExecutor threadExecuter){
        this.todoRepository = todoRepository;
        this.threadExecutor = threadExecuter;
    }
    @Override public void execute(Long id, String title, boolean done){
        this.title = title;
        todoRepository.upsertTodo(id, title, done);
        //this.threadExecutor.execute(this);
    }
    @Override public void run(){
        todoRepository.createTodo(title);
    }
}
