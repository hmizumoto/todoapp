package com.example.hmizumoto.todoapp.data.repository.datasource;

import android.content.Context;

import com.example.hmizumoto.todoapp.data.entity.mapper.DBDataMapper;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TodoDataStoreFactory {
    private Context context;
    @Inject TodoDataStoreFactory(Context context){
        this.context = context;
    }
    public TodoDataStore createDBDataStore(){
        return new DBTodoDataStore(this.context, new DBDataMapper());
    }
}
