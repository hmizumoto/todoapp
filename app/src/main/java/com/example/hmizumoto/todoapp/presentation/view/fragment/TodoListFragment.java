package com.example.hmizumoto.todoapp.presentation.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.example.hmizumoto.todoapp.R;
import com.example.hmizumoto.todoapp.domain.model.TodoModel;
import com.example.hmizumoto.todoapp.presentation.internal.di.components.TodoComponent;
import com.example.hmizumoto.todoapp.presentation.view.TodoListView;
import com.example.hmizumoto.todoapp.presentation.presenter.TodoListPresenter;
import com.example.hmizumoto.todoapp.presentation.view.adapter.TodoAdapter;


import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * Created by hmizumoto on 16/12/05.
 */

public class TodoListFragment extends BaseFragment implements TodoListView {
    @Inject TodoListPresenter todoListPresenter;
    @BindView(R.id.list_view) ListView listview;
    @BindView(R.id.edit_todo) EditText editText;
    private TodoAdapter todoAdapter;
    private TodoListListener todoListListener;
    private Unbinder unbinder;
    private InputMethodManager inputMethodManager;
    public TodoListFragment() {
        super();
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof TodoListListener) {
            this.todoListListener = (TodoListListener) activity;
        }
    }
    @Override public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof TodoListListener){
            this.todoListListener = (TodoListListener) context;
        }

    }

    public void hideKeyboardAndUnforcus(){
        inputMethodManager.hideSoftInputFromWindow(listview.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        listview.requestFocus();
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_todo_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                view.setOnTouchListener( new View.OnTouchListener(){
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboardAndUnforcus();
                return true;
            }
        });
        return view;
    }

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
        loadTodoList();
    }

    @Override public void onResume() {
        super.onResume();
        this.todoListPresenter.resume();
    }

    @Override public void onPause() {
        this.todoListPresenter.pause();
        super.onPause();
    }


    private void init() {
        this.getComponent(TodoComponent.class).inject(this);
        this.todoListPresenter.setView(this);
        this.todoAdapter = new TodoAdapter(getActivity(), new TodoDoneHandler());
        this.listview.setAdapter(this.todoAdapter);
    }


    @Override public void renderTodoList(Collection<TodoModel> todoModels) {
        if (todoModels != null) {
            todoAdapter.clear();
            todoAdapter.addAll(todoModels);
            todoAdapter.notifyDataSetChanged();
        }
    }


    @Override public Context getContext() {
        return this.getActivity().getApplicationContext();
    }

    private void loadTodoList() {
        this.todoListPresenter.loadTodoList();
    }

    @OnClick({R.id.save_todo})
    public void saveTodo() {
        todoListPresenter.saveTodo(editText.getText().toString());
        editText.setText("");
        hideKeyboardAndUnforcus();
        loadTodoList();
    }
    public class TodoDoneHandler {
        public void onCheckboxClicked(TodoModel todoModel){
            todoModel.toggleDone();
            todoListPresenter.updateTodo(todoModel.getId(), todoModel.getTitle(), todoModel.getDone());
        }

    }
    public interface TodoListListener {

    }
}
