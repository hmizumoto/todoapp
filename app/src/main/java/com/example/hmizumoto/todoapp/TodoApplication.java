package com.example.hmizumoto.todoapp;

import android.app.Application;

import com.example.hmizumoto.todoapp.presentation.internal.di.components.ApplicationComponent;
import com.example.hmizumoto.todoapp.presentation.internal.di.components.DaggerApplicationComponent;
import com.example.hmizumoto.todoapp.presentation.internal.di.modules.ApplicationModule;


public class TodoApplication extends Application {
    private ApplicationComponent applicationComponent;
    @Override public void onCreate(){
        super.onCreate();
        injectApplicationComponent();
    }
    private void injectApplicationComponent() {
        this.applicationComponent =
              DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
       this.applicationComponent.inject(this);
    }
    public ApplicationComponent getApplicationComponent(){
        return this.applicationComponent;
    }
}
