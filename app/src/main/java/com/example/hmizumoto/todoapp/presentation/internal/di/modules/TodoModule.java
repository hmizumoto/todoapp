package com.example.hmizumoto.todoapp.presentation.internal.di.modules;

import com.example.hmizumoto.todoapp.domain.usecase.CreateTodoUseCase;
import com.example.hmizumoto.todoapp.domain.usecase.CreateTodoUseCaseImpl;
import com.example.hmizumoto.todoapp.domain.usecase.GetTodoListUseCase;
import com.example.hmizumoto.todoapp.domain.usecase.GetTodoListUseCaseImpl;
import com.example.hmizumoto.todoapp.presentation.internal.di.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hmizumoto on 16/12/05.
 */

@Module
public class TodoModule {
    @Provides @PerActivity
    GetTodoListUseCase provideGetTodoListUseCase(
            GetTodoListUseCaseImpl getTodoListUseCase
    ){
        return getTodoListUseCase;
    }
    @Provides @PerActivity
    CreateTodoUseCase provideCreateTodoUseCase(
            CreateTodoUseCaseImpl createTodoUseCase
    ){
        return createTodoUseCase;
    }
}
