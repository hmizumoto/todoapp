package com.example.hmizumoto.todoapp.data.repository.datasource;


import com.example.hmizumoto.todoapp.domain.model.TodoModel;

import java.util.Collection;

/**
 * Created by hmizumoto on 16/12/02.
 */

public interface TodoDataStore {
    public void getTodoList(TodoListCallback callback);
    public void createTodo(String title);
    public void upsertTodo(Long id, String title, boolean done);
    interface TodoListCallback {
        void onTodoListLoaded(Collection<TodoModel> toods);
    }
}
